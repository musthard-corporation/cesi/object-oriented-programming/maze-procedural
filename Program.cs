﻿const char PLAYER = '&';
const char WALL = '@';
const char EXIT = '"';
const char ROAD = ' ';

RunMaze();

void RunMaze()
{
    Console.WriteLine("Welcome !");
    Console.WriteLine($"Use arrow keys to move player ({PLAYER})");
    Console.WriteLine($"Your goal is to reach the exit ({EXIT})");
    Console.WriteLine($"Your can not pass through walls ({WALL})");
    Console.WriteLine($"If you want to stop game, press '0'");
    Console.WriteLine("Press 'Enter' to continue...");
    Console.Read();

    char[,] grid = InitMaze(6, (0, 0), (5, 5), [(0, 2), (1, 0), (1, 4), (2, 1), (2, 2), (2, 3), (3, 5), (4, 0), (4, 2), (4, 3), (4, 4)]);

    bool exit = false;
    do
    {
        DisplayMaze(grid);
        var key = Console.ReadKey().Key;
        exit = (key == ConsoleKey.D0);
        if (!exit)
        {
            exit = MoveUser(grid, key);
            if (exit)
            {
                Console.WriteLine("\nWell played !\n");
            }
        }
    } while (!exit);
}

char[,] InitMaze(int size = 5, (int, int)? userPosition = null, (int, int)? exitPosition = null, (int, int)[]? walls = null)
{
    char[,] grid = new char[size, size];
    (int, int) user = userPosition ?? (0, 0);
    (int, int) exit = exitPosition ?? (size - 1, size - 1);
    walls = walls ?? [];

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (i == user.Item1 && j == user.Item2)
            {
                grid[i, j] = PLAYER;
            }
            else if (i == exit.Item1 && j == exit.Item2)
            {
                grid[i, j] = EXIT;
            }
            else if (walls.Any(w => w.Item1 == i && w.Item2 == j))
            {
                grid[i, j] = WALL;
            }
            else
            {
                grid[i, j] = ROAD;
            }
        }
    }

    return grid;
}

void DisplayMaze(char[,] grid)
{
    Console.Clear();
    Console.WriteLine();

    for (int i = 0; i < grid.GetLength(0); i++)
    {
        for (int j = 0; j < grid.GetLength(1); j++)
        {
            Console.Write($"|{grid[i, j]}");
        }
        Console.WriteLine("|");
    }
}

bool MoveUser(char[,] grid, ConsoleKey direction)
{
    var player = GetPlayerPosition(grid);
    var exit = GetExitPosition(grid);

    if (player == (-1, -1) || exit == (-1, -1))
    {
        return false;
    }

    var destination = player;

    switch (direction)
    {
        case ConsoleKey.DownArrow:
            if (player.Item1 < grid.GetLength(0) - 1 && grid[player.Item1 + 1, player.Item2] != WALL)
            {
                destination.Item1++;
            }
            break;
        case ConsoleKey.LeftArrow:
            if (player.Item2 > 0 && grid[player.Item1, player.Item2 - 1] != WALL)
            {
                destination.Item2--;
            }
            break;
        case ConsoleKey.RightArrow:
            if (player.Item2 < grid.GetLength(1) - 1 && grid[player.Item1, player.Item2 + 1] != WALL)
            {
                destination.Item2++;
            }
            break;
        case ConsoleKey.UpArrow:
            if (player.Item1 > 0 && grid[player.Item1 - 1, player.Item2] != WALL)
            {
                destination.Item1--;
            }
            break;
    }

    grid[player.Item1, player.Item2] = ROAD;
    grid[destination.Item1, destination.Item2] = PLAYER;

    return destination == exit;
}

(int, int) GetPlayerPosition(char[,] grid)
{
    for (int i = 0; i < grid.GetLength(0); i++)
    {
        for (int j = 0; j < grid.GetLength(1); j++)
        {
            if (grid[i, j] == PLAYER)
            {
                return (i, j);
            }
        }
    }

    return (-1, -1);
}

(int, int)? GetExitPosition(char[,] grid)
{
    for (int i = 0; i < grid.GetLength(0); i++)
    {
        for (int j = 0; j < grid.GetLength(1); j++)
        {
            if (grid[i, j] == EXIT)
            {
                return (i, j);
            }
        }
    }

    return (-1, -1);
}
